import React from "react";
import {Box, Text, Heading} from '@chakra-ui/react';

const page401: React.FC = (): JSX.Element => {
    return (
        <Box>
            <Heading>401</Heading>
        <Text>Авторизуйтесь для перегляду</Text>
    </Box>
)}

export default page401;
