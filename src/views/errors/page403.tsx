import React from "react";
import {Box, Text, Heading} from '@chakra-ui/react';
const page404: React.FC = (): JSX.Element => {
    return (
        <Box>
            <Heading>403</Heading>
        <Text>Доступ заборонено</Text>
    </Box>
)
}

export default page404;
